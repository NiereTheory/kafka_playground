/opt/kafka_2.12-2.4.0/bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic mytopic
/opt/kafka_2.12-2.4.0/bin/kafka-topics.sh --list --zookeeper zookeeper:2181
/opt/kafka_2.12-2.4.0/bin/kafka-console-producer.sh --topic mytopic --broker-list kafka:9092
/opt/kafka_2.12-2.4.0/bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic mytopic --from-beginning