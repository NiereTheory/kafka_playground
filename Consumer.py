from typing import List
import time

from kafka import KafkaConsumer, TopicPartition

MAX_BATCH_SIZE = 10
TOPIC = "mytopic"
consumer = KafkaConsumer(
    # "mytopic",
    bootstrap_servers=["kafka:9092"],
    group_id="test_group",
    auto_offset_reset="earliest",
    enable_auto_commit=False,
    value_deserializer=lambda x: x.decode("utf-8"),
    # consumer_timeout_ms=10000,
    max_poll_records=MAX_BATCH_SIZE,
)

print(consumer.partitions_for_topic(TOPIC))


tp = TopicPartition(topic=TOPIC, partition=0)
consumer.assign([tp])
# consumer.seek(tp, 0)
consumer.seek_to_beginning(tp)


def handle_messages(messages: List) -> None:
    print([message.value for message in messages], len(messages))


msg_list = []
try:
    while True:
        # print(consumer.assignment())
        msg = consumer.poll(timeout_ms=5000)
        if msg:
            msg_list.extend(msg[tp])
            if len(msg_list) % MAX_BATCH_SIZE == 0:
                handle_messages(msg_list)
                consumer.commit()
                msg_list.clear()
        else:
            print("no messages received this poll")
finally:
    consumer.close()
