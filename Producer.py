import time
import random

from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers="kafka:9092", value_serializer=str.encode)

words = ["use", "kafka", "streaming", "data", "nrt", "python"]
topic = "mytopic"

while True:
    word = random.choice(words)
    producer.send(topic, word)
    print(f"{word} published to {topic}")
    time.sleep(0.5)
    producer.flush()
